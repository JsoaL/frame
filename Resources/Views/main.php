<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Frame-PWA</title>
    <meta name="description" content="Awesome App Template built with Bootstrap Framework and Paperkit for creating App Landing Pages by TemplateFlip.com"/>
    <link href="https://fonts.googleapis.com/css?family=Dosis:600|Roboto:400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="Resources/CSS/bootstrap.min.css" rel="stylesheet">
    <link href="Resources/CSS/paper-kit.css?v=2.1.0" rel="stylesheet">
    <link href="Resources/Styles/main.css" rel="stylesheet">
  </head>
  <body id="top">
    <header>
      <div class="aa-header">
        <nav class="navbar navbar-expand-md navbar-transparent">
          <div class="container"><img class="img-fluid pr-3 aa-logo-img" src="Resources/Images/logo.png" alt="logo"><a class="navbar-brand px-0 py-0" href="#">Framework PWA 2019</a>
            <div class="collapse navbar-collapse">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="#">Inicio</a></li>
                <li class="nav-item"><a class="nav-link" href="#features">Detalles</a></li>
                <li class="nav-item"><a class="nav-link" href="#installation">Instalación</a></li>
                <li class="nav-item"><a class="btn btn-outline-neutral btn-round" href="https://gitlab.com/JsoaL/frame/-/archive/master/frame-master.zip">Descargar</a></li>
              </ul>
            </div>
          </div>
        </nav>
        <div class="container aa-header-content text-left text-white">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <h1 class="text-white mb-4"> Programacion Web Avanzada <br/>Primer ejercicio</h1>
              <p>Creado para comprender a detalle el modelo vista controlador</p>
              <ul class="py-1 list-unstyled">
                <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> Repositorio en linea</li>
                <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> Instalacion apartir de un script &amp; PaperKit</li>
                <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> Funcion de un esquema ORM personalizado</li>
              </ul><a class="mt-4 btn btn-outline-neutral btn-round" href="https://gitlab.com/JsoaL/frame.git">Repositorio en Gitlab</a>
            </div>
            <div class="col-md-6 col-sm-12 text-right"><img class="img-fluid" src="Resources/Images/2.png" alt="Image"></div>
          </div>
        </div>
      </div>
    </header>
    <div class="page-content">
      <div class="container">
          <!-- Contenido -->
      </div>
    <div>
    <script src="Resources/JS/jquery-3.2.1.min.js"></script>
    <script src="Resources/JS/jquery-ui-1.12.1.custom.min.js"></script>
    <script src="Resources/JS/popper.js"></script>
    <script src="Resources/JS/bootstrap.min.js"></script>
    <script src="Resources/JS/paper-kit.js?v=2.1.0"></script>
    <script src="Resources/Scripts/main.js"></script>
  </body>
</html>
